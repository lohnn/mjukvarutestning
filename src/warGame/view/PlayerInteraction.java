package warGame.view;

import warGame.model.AI;
import warGame.model.GameState;
import warGame.model.Player;
import warGame.model.Regiment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by jl222xa on 2014-11-12.
 */
public class PlayerInteraction {
    PrintStream console;
    BufferedReader scanner;
    public Random random = new Random();

    HashMap<Regiment.UnitType, String> regimentOutputMap = new HashMap<>();

    //TODO: Empty constructor!
    public PlayerInteraction(PrintStream console, BufferedReader scanner) {
        this.console = console;
        this.scanner = scanner;
        regimentOutputMap.put(Regiment.UnitType.Archers, "\u2650");
        regimentOutputMap.put(Regiment.UnitType.Cavalry, "\u265E");
        regimentOutputMap.put(Regiment.UnitType.Cannons, "\u2604");
        regimentOutputMap.put(Regiment.UnitType.Spearmen, "\u2191");
        regimentOutputMap.put(Regiment.UnitType.Swordsmen, "\u2694");
    }

    public PlayerInteraction() {
        this(System.out, new BufferedReader(new InputStreamReader(System.in)));
    }

    public boolean validateInput(String one) {
        try {
            return validateInput(Integer.parseInt(one));
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean validateInput(int iOne) {
        if (iOne > 0 && iOne <= Regiment.UnitType.values().length)
            return true;
        return false;
    }

    public Regiment getUnitFromConsole(ArrayList<Regiment> regiments) {
        while (true) {
            Player player = new Player(regiments.size());
            player.setRegiments(regiments);
            unitViewPrinter(player);
            console.println("Choose your next regiment: 1[Cavalry] 2[Cannons] 3[Archers] 4[Swordsmen] 5[Spearmen]");
            try {
                String s = scanner.readLine();
                if (!validateInput(s))
                    throw new IllegalArgumentException("Not valid input");

                return new Regiment(Regiment.UnitType.values()[Integer.parseInt(s) - 1]);
            } catch (IllegalArgumentException e) {
                console.println(e.getMessage() + ", try again:");
            } catch (Exception e) {
            }
        }
    }

    public ArrayList<Regiment> getRegimentsFromUser() {
        ArrayList<Regiment> l = new ArrayList();
        for (int i = 0; i < 10; i++)
            l.add(getUnitFromConsole(l));
        return l;
    }

    public void printNewGameScreen() {
        console.println("Welcome to the best game in the world. Press enter to start.");
        try {
            scanner.readLine();
        } catch (IOException e) {
        }
    }

    public boolean printGameOver(GameState state) {
        String s = "Something bad happened, I don't really know who won.";
        try {
            switch (state) {
                case GameDraw:
                    s = "Not good enough, game ended in a draw.";
                    break;
                case GameLost:
                    s = "Haha you lost!";
                    break;
                case GameWon:
                    s = "Congratulations you beat the bad computer.";
                    break;
            }
        } catch (Exception e) {
        }
        console.println(s + "\nDo you want to play again? (y/n)");

        while (true) {
            try {
                s = scanner.readLine();
                if (s.toLowerCase().equals("y"))
                    return true;
                else if (s.toLowerCase().equals("n"))
                    return false;
                console.println("Invalid input, please try again (You just have to write \"y\" or \"n\"." +
                        " Can it be so fucking hard?)" +
                        "\nDo you want to play again?(y/n)");
            } catch (IOException e) {
            }
        }
    }

    public void unitViewPrinter(Player p) {
        Regiment[] r = p.getRegiments();

        StringBuilder sb = new StringBuilder();

        if (p.getClass() == AI.class) {
            sb.append("Enemy Army: ");
            ArrayList<Integer> randomList = new ArrayList<>();
            randomList.add(randomAiRegiment(randomList));
            randomList.add(randomAiRegiment(randomList));
            randomList.add(randomAiRegiment(randomList));

            for (int i = 0; i < 10; i++) {
                if (randomList.contains(i))
                    sb.append(String.format("[%s]", r[i].getUnitType()));
                else
                    sb.append("[?]");
            }
        } else {
            sb.append("Your Army: ");
            for (Regiment reg : r) {
                sb.append(String.format("[%s]", reg.getUnitType()));
            }
        }

        console.println(sb.toString());
    }

    public int randomAiRegiment(List<Integer> oldValues) {
        if (oldValues.size() > 10)
            throw new IndexOutOfBoundsException("Cannot randomize a number that's not already used.");

        int toReturn = random.nextInt(10);
        while (oldValues.contains(toReturn))
            toReturn = random.nextInt(10);
        return toReturn;
    }

    public void fightFeedback(Player p1, Player p2) {
        StringBuilder sb = new StringBuilder();
        sb.append("Player: ");
        for (int i = p1.getRegiments().length - 1; i >= 0; i--) {
            Regiment r = p1.getRegiments()[i];
            sb.append("[");
            for (int j = 0; j < r.unitsLeft() / 25; j++)
                sb.append(regimentOutputMap.get(r.getUnitType()));
            sb.append("]");
        }

        sb.append(" -- ");

        for (Regiment r : p2.getRegiments()) {
            sb.append("[");
            for (int j = 0; j < r.unitsLeft() / 25; j++)
                sb.append(regimentOutputMap.get(r.getUnitType()));
            sb.append("]");
        }

        sb.append(" :Computer");

        console.println(sb.toString());
        try {
            sleep();
        } catch (InterruptedException e) {
        }
    }

    public void sleep() throws InterruptedException {
        Thread.sleep(1000);
    }
}
