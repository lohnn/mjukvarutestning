package warGame.model;

import java.util.ArrayList;

/**
 * Created by jl222xa on 2014-11-07.
 */
public class Player {
    //TODO: Change back to protected
    public int regimentAmount;
    ArrayList<Regiment> regiments = new ArrayList<>();

    public Player(int regimentAmount) {
        this.regimentAmount = regimentAmount;
    }

    public void addRegiment(Regiment regiment) {
        if (regiments.contains(regiment))
            throw new IllegalArgumentException("Cannot add same regiment twice (duplicate)");
        if (regiments.size() + 1 > regimentAmount)
            throw new IndexOutOfBoundsException("Array is full!");

        regiments.add(regiment);
    }

    public Regiment[] getRegiments() {
        Regiment[] regArr = new Regiment[regiments.size()];
        return regiments.toArray(regArr);
    }

    public void setRegiments(ArrayList<Regiment> reg) {
        regiments = (reg.size() > regimentAmount)
                ? new ArrayList<>(reg.subList(0, 10))
                : reg;
    }

    public boolean removeRegiment(Regiment regiment) {
        if (regiments.contains(regiment)) {
            if (regiment.unitsLeft() > 0)
                throw new RuntimeException("Regiment isn't exterminated");

            regiments.remove(regiment);
            return true;
        } else
            return false;
    }
}
