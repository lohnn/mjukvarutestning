package warGame.model;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by jl222xa on 2014-11-13.
 */
public class AI extends Player {
    Random rand = new Random();

    public AI(int amount) {
        super(amount);
        generateRegiments();
    }

    public AI(Random rand) {
        this(10);
        this.rand = rand;
    }

    public Regiment[] generateRegiments() {
        regiments.clear();
        for (int i = 0; i < regimentAmount; i++) {
            addRegiment(new Regiment(Regiment.UnitType.values()[rand.nextInt(Regiment.UnitType.values().length)]));
        }
        return getRegiments();
    }
}
