package warGame.model;

/**
 * Created by jl222xa on 2014-11-11.
 */
public class GameRules {
    Player human, computer;
    BattleRules battleRules = new BattleRules();
    private GameState gameState;

    public void newGame(Player human, Player computer) {
        this.human = human;
        this.computer = computer;
    }

    //TODO: Refactor to returning GameState instead of Boolean perhaps?
    public boolean makeMove() {
        if (computer.getRegiments().length <= 0
                || human.getRegiments().length <= 0)
            return true;

        battleRules.fight(computer.getRegiments()[0], human.getRegiments()[0]);
        if (computer.getRegiments()[0].unitsLeft() <= 0)
            computer.removeRegiment(computer.getRegiments()[0]);
        if (human.getRegiments()[0].unitsLeft() <= 0)
            human.removeRegiment(human.getRegiments()[0]);

        if (computer.getRegiments().length > 0 && human.getRegiments().length <= 0) {
            gameState = GameState.GameLost;
        } else if (human.getRegiments().length > 0 && computer.getRegiments().length <= 0)
            gameState = GameState.GameWon;
        else if (computer.getRegiments().length <= 0 && human.getRegiments().length <= 0)
            gameState = GameState.GameDraw;

        return false;
    }

    public GameState getGameState() {
        return gameState;
    }
}
