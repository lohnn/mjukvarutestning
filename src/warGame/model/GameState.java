package warGame.model;

/**
 * Created by jl222xa on 2014-11-14.
 */
public enum GameState {
    GameDraw, GameWon, GameLost
}
