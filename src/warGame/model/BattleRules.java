package warGame.model;

public class BattleRules {

    public void fight(Regiment one, Regiment two) {
        one.damage(calculateDamage(two.getUnitType(), one.getUnitType()));
        two.damage(calculateDamage(one.getUnitType(), two.getUnitType()));
    }

    private int calculateDamage(Regiment.UnitType attacker, Regiment.UnitType defender) {
        switch (attacker) {
            case Archers:
                if (defender == Regiment.UnitType.Swordsmen)
                    return 50; //TODO: Move these values to a static? variable.
                return 25;
            case Cavalry:
                if (defender == Regiment.UnitType.Cannons)
                    return 50;
                return 25;
            case Cannons:
                if (defender == Regiment.UnitType.Archers)
                    return 50;
                return 25;
            case Spearmen:
                if (defender == Regiment.UnitType.Cavalry)
                    return 50;
                return 25;
            case Swordsmen:
                if (defender == Regiment.UnitType.Spearmen)
                    return 50;
                return 25;
            default:
                throw new RuntimeException("Invalid UnitType, does not exist.");
        }
    }
}
