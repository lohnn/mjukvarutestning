package warGame.model;

public class Regiment {
    public enum UnitType {
        Cavalry,
        Cannons,
        Archers,
        Swordsmen,
        Spearmen
    }

    private UnitType unitType;
    private int unitsLeft;

    public Regiment(UnitType unitType) {
        this.unitType = unitType;
        unitsLeft = 100;
    }

    public UnitType getUnitType() {
        return unitType;
    }

    public int unitsLeft() {
        return unitsLeft;
    }

    public void damage(int i) {
        if (i < 0)
            throw new IllegalArgumentException("Cannot damage negative values");
        unitsLeft -= i;
    }


}
