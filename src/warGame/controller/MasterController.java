package warGame.controller;

import warGame.model.*;
import warGame.view.PlayerInteraction;

import java.util.ArrayList;

/**
 * Created by jl222xa on 2014-11-14.
 */
public class MasterController {
    private PlayerInteraction view;
    private GameRules model;
    private GameState gameState;

    public MasterController(GameRules model, PlayerInteraction view) {
        this.view = view;
        this.model = model;
    }

    public void startMainLoop() {
        while (mainLoopTick()) ;
    }

    //TODO: Refactor to private
    public boolean mainLoopTick() {
        view.printNewGameScreen();
        Player comp = createNewAI();
        view.unitViewPrinter(comp);
        ArrayList<Regiment> regiments = view.getRegimentsFromUser();
        Player player = createNewPlayer(regiments);
        model.newGame(player, comp);
        while (!model.makeMove()) view.fightFeedback(player, comp);
        gameState = model.getGameState();
        return view.printGameOver(gameState);
    }

    public Player createNewPlayer(ArrayList<Regiment> regiments) {
        Player player = new Player(10);
        player.setRegiments(regiments);
        return player;
    }

    public AI createNewAI() {
        return new AI(10);
    }
}
