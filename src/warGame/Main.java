package warGame;

import warGame.controller.MasterController;
import warGame.model.GameRules;
import warGame.view.PlayerInteraction;

/**
 * Created by jl222xa on 2014-11-14.
 */
public class Main {
    public static void main(String[] args) {
        GameRules model = new GameRules();
        PlayerInteraction view = new PlayerInteraction();
        MasterController controller = new MasterController(model, view);
        controller.startMainLoop();
    }
}
