package warGameTests.model;

import org.junit.Before;
import org.junit.Test;
import warGame.model.AI;
import warGame.model.Player;
import warGame.model.Regiment;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.*;

/**
 * Created by jl222xa on 2014-11-13.
 */
public class AITest {

    AI ai;

    @Before
    public void initialize() {
        ai = new AI(10);
    }

    @Test
    public void testGenerateRegiments() {
        Random rand = mock(Random.class);
        ai = new AI(rand);

        when(rand.nextInt(5)).
                thenReturn(0).
                thenReturn(1).
                thenReturn(2).
                thenReturn(3).
                thenReturn(4).
                thenReturn(4).
                thenReturn(3).
                thenReturn(2).
                thenReturn(1).
                thenReturn(0);


        Regiment[] reg = ai.generateRegiments();
        Regiment[] reg2 = ai.getRegiments();
        assertEquals(10, reg.length);
        assertEquals(10, reg2.length);

        for (Regiment r : reg)
            assertNotEquals(null, r);

        assertEquals(reg, reg2);
        verify(rand, times(10)).nextInt(5);
    }


}
