package warGameTests.model;

import org.junit.Assert;
import org.junit.Test;
import warGame.model.BattleRules;
import warGame.model.Regiment;
import warGame.model.Regiment.UnitType;

public class BattleRulesTest {
    private BattleRules br = new BattleRules();

    @Test
    public void testAttack() {
        testAttack(UnitType.Cavalry, UnitType.Cannons);
        testAttack(UnitType.Cannons, UnitType.Archers);
        testAttack(UnitType.Archers, UnitType.Swordsmen);
        testAttack(UnitType.Swordsmen, UnitType.Spearmen);
        testAttack(UnitType.Spearmen, UnitType.Cavalry);

        testAttackEquals(UnitType.Cavalry, UnitType.Archers);
        testAttackEquals(UnitType.Cavalry, UnitType.Swordsmen);

        testAttackEquals(UnitType.Cannons, UnitType.Swordsmen);
        testAttackEquals(UnitType.Cannons, UnitType.Spearmen);

        testAttackEquals(UnitType.Archers, UnitType.Spearmen);
    }

    private void testAttack(UnitType strong, UnitType weak){
        Regiment s = new Regiment(strong);
        Regiment w = new Regiment(weak);

        br.fight(s, w);

        Assert.assertEquals(75, s.unitsLeft());
        Assert.assertEquals(50, w.unitsLeft());
    }
    private void testAttackEquals(UnitType one, UnitType two){
        Regiment o = new Regiment(one);
        Regiment t = new Regiment(two);

        br.fight(o, t);

        Assert.assertEquals(75, o.unitsLeft());
        Assert.assertEquals(75, t.unitsLeft());
    }

}
