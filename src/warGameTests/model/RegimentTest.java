package warGameTests.model;

import org.junit.Assert;
import org.junit.Test;
import warGame.model.Regiment;
import warGame.model.Regiment.UnitType;

public class RegimentTest {

    @Test
    public void testConstructor() {
        Regiment regiment = new Regiment(UnitType.Spearmen);
    }

    @Test
    public void testUnitType() {
        Regiment regiment = new Regiment(UnitType.Swordsmen);

        Assert.assertTrue(regiment.getUnitType() == UnitType.Swordsmen);

        regiment = new Regiment(UnitType.Archers);
        Assert.assertTrue(regiment.getUnitType() == UnitType.Archers);
    }

    @Test
    public void testUnitAmountOnConstructor() {
        Regiment regiment = new Regiment(UnitType.Cavalry);

        Assert.assertTrue(regiment.unitsLeft() == 100);
    }

    @Test
    public void testDamage() {
        Regiment regiment = new Regiment(UnitType.Archers);

        regiment.damage(25);
        Assert.assertTrue(regiment.unitsLeft() == 75);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDamageNegative() {
        Regiment regiment = new Regiment(UnitType.Spearmen);
        regiment.damage(-25);
    }
}
