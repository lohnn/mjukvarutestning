package warGameTests.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import warGame.model.Player;
import warGame.model.Regiment;

import java.util.ArrayList;

public class PlayerTest {

    private Player p;

    @Before
    public void initPlayer() {
        p = new Player(10);
    }

    @Test
    public void testAddRegiment() throws Exception {
        Regiment cavalry = new Regiment(Regiment.UnitType.Cavalry);
        p.addRegiment(cavalry);
        Assert.assertArrayEquals(new Regiment[]{cavalry}, p.getRegiments());

        Regiment archers = new Regiment(Regiment.UnitType.Archers);
        p.addRegiment(archers);
        Assert.assertArrayEquals(new Regiment[]{cavalry, archers}, p.getRegiments());
    }

    @Test(expected = Exception.class)
    public void testAddDuplicate() {
        Regiment cavalry = new Regiment(Regiment.UnitType.Cavalry);
        p.addRegiment(cavalry);
        p.addRegiment(cavalry);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testAddTooMany() {
        for (int i = 0; i <= 10; i++) {
            Regiment cavalry = new Regiment(Regiment.UnitType.Cavalry);
            p.addRegiment(cavalry);
        }
    }

    @Test(expected = Exception.class)
    public void testRemoveNotDead() {
        Regiment archers = new Regiment(Regiment.UnitType.Archers);
        p.addRegiment(archers);
        p.removeRegiment(archers);
    }

    @Test
    public void testRemoveNotExist() {
        Regiment archers = new Regiment(Regiment.UnitType.Archers);
        Assert.assertFalse(p.removeRegiment(archers));
    }

    @Test
    public void testRemoveRegiment() {
        Regiment archers = new Regiment(Regiment.UnitType.Archers);
        p.addRegiment(archers);
        archers.damage(100);
        Assert.assertTrue(p.removeRegiment(archers));
        Assert.assertTrue(p.getRegiments().length == 0);
    }

    @Test
    public void testSetRegiments() {
        ArrayList<Regiment> reg = new ArrayList<>();
        Regiment r1 = new Regiment(Regiment.UnitType.Archers);
        Regiment r2 = new Regiment(Regiment.UnitType.Swordsmen);

        reg.add(r1);
        reg.add(r2);
        p.setRegiments(reg);

        Assert.assertEquals(r1, p.getRegiments()[0]);
        Assert.assertEquals(r2, p.getRegiments()[1]);
    }

    @Test
    public void testSetRegimentsInvalid() {
        ArrayList<Regiment> reg = new ArrayList<>();

        for (int i = 0; i <= 11; i++)
            reg.add(new Regiment(Regiment.UnitType.Cannons));

        p.setRegiments(reg);

        Assert.assertEquals(p.regimentAmount, p.getRegiments().length);
    }
}