package warGameTests.model;

import org.junit.Assert;
import org.junit.Test;
import warGame.model.GameRules;
import warGame.model.GameState;
import warGame.model.Player;
import warGame.model.Regiment;

public class GameRulesTest {

    //fields
    private GameRules rules = new GameRules();
    private Player player;
    private Player comp;

    @Test
    public void testGetHumanAndComp() {
        player = new Player(10);
        comp = new Player(10);
        rules.newGame(player, comp);
    }

    @Test
    public void testResultAfterMove() {
        player = new Player(10);
        comp = new Player(10);
        rules.newGame(player, comp);

        player.addRegiment(new Regiment(Regiment.UnitType.Cavalry));
        comp.addRegiment(new Regiment(Regiment.UnitType.Cannons));

        rules.makeMove();

        Assert.assertTrue(player.getRegiments()[0].unitsLeft() > comp.getRegiments()[0].unitsLeft());
    }

    @Test(timeout = 1000)
    public void testMakeMoveWithTenRegiments() {
        generateNewGameRulesWithTwoRegiments(new Regiment(Regiment.UnitType.Cavalry), new Regiment(Regiment.UnitType.Archers));

        for (int i = 0; i < 9; i++)
            player.addRegiment(new Regiment(Regiment.UnitType.Cavalry));
        for (int i = 0; i < 9; i++)
            comp.addRegiment(new Regiment(Regiment.UnitType.Cannons));

        while (!rules.makeMove()) ;

        Assert.assertEquals(player.getRegiments().length, 5);
        Assert.assertEquals(GameState.GameWon, rules.getGameState());
    }


    @Test(timeout = 1000)
    public void testWinner() {
        generateNewGameRulesWithTwoRegiments(new Regiment(Regiment.UnitType.Cavalry), new Regiment(Regiment.UnitType.Cannons));

        while (!rules.makeMove()) ;

        Assert.assertTrue(player.getRegiments().length > comp.getRegiments().length);
        Assert.assertTrue(player.getRegiments()[0].unitsLeft() == 50);

        generateNewGameRulesWithTwoRegiments(new Regiment(Regiment.UnitType.Spearmen), new Regiment(Regiment.UnitType.Swordsmen));

        Assert.assertEquals(GameState.GameWon, rules.getGameState());

        while (!rules.makeMove()) ;

        Assert.assertTrue(player.getRegiments().length < comp.getRegiments().length);
        Assert.assertTrue(comp.getRegiments()[0].unitsLeft() == 50);
        //Test game state
        Assert.assertEquals(GameState.GameLost, rules.getGameState());
    }

    @Test(timeout = 1000)
    public void testNoWinner() {
        generateNewGameRulesWithTwoRegiments(new Regiment(Regiment.UnitType.Cavalry), new Regiment(Regiment.UnitType.Swordsmen));

        while (!rules.makeMove()) ;

        Assert.assertEquals(player.getRegiments().length, comp.getRegiments().length);
        Assert.assertEquals(GameState.GameDraw, rules.getGameState());
    }

    private void generateNewGameRulesWithTwoRegiments(Regiment playerRegiment, Regiment computerRegiment) {
        player = new Player(10);
        comp = new Player(10);
        rules.newGame(player, comp);

        player.addRegiment(playerRegiment);
        comp.addRegiment(computerRegiment);
    }
}