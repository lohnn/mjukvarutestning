package warGameTests.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import warGame.controller.MasterController;
import warGame.model.*;
import warGame.view.PlayerInteraction;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

public class MasterControllerTest {
    MasterController mc;
    GameRules model;
    PlayerInteraction view;

    @Before
    public void init() {
        model = mock(GameRules.class);
        view = mock(PlayerInteraction.class);
        mc = new MasterController(model, view);
    }

    @Test
    public void testCreateNewPlayer() {
        ArrayList<Regiment> regiments = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            regiments.add(new Regiment(Regiment.UnitType.Spearmen));
        }
        Player p = mc.createNewPlayer(regiments);
        Regiment[] r = p.getRegiments();
        for (int i = 0; i < 10; i++) {
            Assert.assertEquals(r[i], regiments.get(i));
        }
    }

    @Test
    public void testMainLoop() {
        mc = spy(mc);
        InOrder inOrder = inOrder(view, model, mc);

        ArrayList<Regiment> playerRegiments = new ArrayList<>();
        ArrayList<Regiment> compRegiment = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            playerRegiments.add(new Regiment(Regiment.UnitType.Cannons));
            compRegiment.add(new Regiment(Regiment.UnitType.Cavalry));
        }

        Player player = new Player(10);
        player.setRegiments(playerRegiments);
        AI comp = mc.createNewAI();
        comp.setRegiments(compRegiment);

        doReturn(player).when(mc).createNewPlayer(any(ArrayList.class));
        doReturn(comp).when(mc).createNewAI();
        when(view.getRegimentsFromUser()).thenReturn(playerRegiments);
        when(model.makeMove())
                .thenReturn(false)
                .thenReturn(false)
                .thenReturn(true);
        when(model.getGameState()).thenReturn(GameState.GameLost);
        when(view.printGameOver(any(GameState.class)))
                .thenReturn(true)
                .thenReturn(false);

        mc.startMainLoop();

        verify(mc, atLeast(2)).mainLoopTick();
        verify(model, atLeast(4)).makeMove();
        verify(view, atLeast(2)).fightFeedback(player, comp);

        inOrder.verify(mc).mainLoopTick();
        inOrder.verify(view).printNewGameScreen();
        inOrder.verify(mc).createNewAI();
        inOrder.verify(view).unitViewPrinter(comp);
        inOrder.verify(view).getRegimentsFromUser();
        inOrder.verify(mc).createNewPlayer(playerRegiments);
        inOrder.verify(model).newGame(player, comp);
        inOrder.verify(model).makeMove();
        inOrder.verify(view).fightFeedback(player, comp);
        inOrder.verify(model).getGameState();
        inOrder.verify(view).printGameOver(GameState.GameLost);
    }
}