package warGameTests.view;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import warGame.model.AI;
import warGame.model.GameState;
import warGame.model.Player;
import warGame.model.Regiment;
import warGame.view.PlayerInteraction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class PlayerInteractionTest {
    PrintStream console;
    BufferedReader scanner;
    PlayerInteraction pi;

    @Before
    public void init() {
        console = mock(PrintStream.class);
        scanner = mock(BufferedReader.class);
        pi = new PlayerInteraction(console, scanner);
    }

    @Test
    public void testNoErrorOnDefaultConstructor() {
        pi = new PlayerInteraction();
    }

    @Test
    public void testMockPrintNewGame() throws IOException {
        when(scanner.readLine()).thenReturn("");
        pi.printNewGameScreen();

        verify(console).println("Welcome to the best game in the world. Press enter to start.");
        verify(scanner).readLine();
    }

    @Test
    public void testMockGameOver() throws IOException {
        when(scanner.readLine()).thenReturn("n");
        pi.printGameOver(GameState.GameLost);
        verify(console).println("Haha you lost!\n" +
                "Do you want to play again? (y/n)");

        pi.printGameOver(GameState.GameWon);
        verify(console).println("Congratulations you beat the bad computer.\n" +
                "Do you want to play again? (y/n)");

        pi.printGameOver(GameState.GameDraw);
        verify(console).println("Not good enough, game ended in a draw.\n" +
                "Do you want to play again? (y/n)");
    }

    @Test
    public void testGameOverRestart() throws IOException {
        when(scanner.readLine()).thenReturn("h").thenReturn("y").thenReturn("n");
        boolean go = pi.printGameOver(GameState.GameLost);
        assertTrue(go);

        verify(console).println("Invalid input, please try again (You just have to write \"y\" or \"n\"." +
                " Can it be so fucking hard?)" +
                "\nDo you want to play again?(y/n)");
        verify(scanner, atLeastOnce()).readLine();

        go = pi.printGameOver(GameState.GameLost);
        assertFalse(go);
    }

    @Test
    public void testGameOverInvalidInput() throws IOException {
        when(scanner.readLine()).thenReturn("n");
        pi.printGameOver(null);
    }

    @Test
    public void mockGetInputFromUser() throws IOException {
        when(scanner.readLine()).thenReturn("1").thenReturn("2").thenReturn("3").thenReturn("4").thenReturn("5");

        ArrayList<Regiment> rList = new ArrayList<>();

        assertRegimentFromInput(Regiment.UnitType.Cavalry, rList);
        assertRegimentFromInput(Regiment.UnitType.Cannons, rList);
        assertRegimentFromInput(Regiment.UnitType.Archers, rList);
        assertRegimentFromInput(Regiment.UnitType.Swordsmen, rList);
        assertRegimentFromInput(Regiment.UnitType.Spearmen, rList);

        InOrder inOrder = inOrder(console);

        inOrder.verify(console).println("Your Army: ");
        inOrder.verify(console).println("Your Army: [Cavalry]");
        inOrder.verify(console).println("Choose your next regiment: 1[Cavalry] 2[Cannons] 3[Archers] 4[Swordsmen] 5[Spearmen]");
        inOrder.verify(console).println("Your Army: [Cavalry][Cannons]");
        inOrder.verify(console).println("Your Army: [Cavalry][Cannons][Archers]");
        inOrder.verify(console).println("Your Army: [Cavalry][Cannons][Archers][Swordsmen]");

        verify(console, times(5)).println("Choose your next regiment: 1[Cavalry] 2[Cannons] 3[Archers] 4[Swordsmen] 5[Spearmen]");
        verify(scanner, times(5)).readLine();
    }

    @Test(timeout = 2000)
    public void mockGetInputAllRegimentsFromUser() {
        pi = spy(pi);

        doReturn(new Regiment(Regiment.UnitType.Cavalry)).
                doReturn(new Regiment(Regiment.UnitType.Archers)).
                doReturn(new Regiment(Regiment.UnitType.Cannons)).
                doReturn(new Regiment(Regiment.UnitType.Archers)).
                doReturn(new Regiment(Regiment.UnitType.Swordsmen)).
                doReturn(new Regiment(Regiment.UnitType.Swordsmen)).
                doReturn(new Regiment(Regiment.UnitType.Spearmen)).
                doReturn(new Regiment(Regiment.UnitType.Cavalry)).
                doReturn(new Regiment(Regiment.UnitType.Cavalry)).
                doReturn(new Regiment(Regiment.UnitType.Cavalry)).when(pi).getUnitFromConsole(any(ArrayList.class));

        Regiment[] testArray = new Regiment[]{
                new Regiment(Regiment.UnitType.Cavalry),
                new Regiment(Regiment.UnitType.Archers),
                new Regiment(Regiment.UnitType.Cannons),
                new Regiment(Regiment.UnitType.Archers),
                new Regiment(Regiment.UnitType.Swordsmen),
                new Regiment(Regiment.UnitType.Swordsmen),
                new Regiment(Regiment.UnitType.Spearmen),
                new Regiment(Regiment.UnitType.Cavalry),
                new Regiment(Regiment.UnitType.Cavalry),
                new Regiment(Regiment.UnitType.Cavalry),
        };

        List<Regiment> regimentList = pi.getRegimentsFromUser();

        //Check if arrays are correct
        for (int i = 0; i < 10; i++) {
            assertEquals(testArray[i].getUnitType(), regimentList.get(i).getUnitType());
        }

        //Check if called 10 times
        verify(pi, times(10)).getUnitFromConsole(any(ArrayList.class));
    }

    @Test
    public void testValidateInputValidData() {
        //tests with strings
        String one = "1", two = "2", three = "3", four = "4", five = "5";

        assertTrue(pi.validateInput(one));
        assertTrue(pi.validateInput(two));
        assertTrue(pi.validateInput(three));
        assertTrue(pi.validateInput(four));
        assertTrue(pi.validateInput(five));

        //tests with integers
        int iOne = 1, iTwo = 2, iThree = 3, iFour = 4, iFive = 5;

        assertTrue(pi.validateInput(iOne));
        assertTrue(pi.validateInput(iTwo));
        assertTrue(pi.validateInput(iThree));
        assertTrue(pi.validateInput(iFour));
        assertTrue(pi.validateInput(iFive));
    }

    @Test
    public void testValidateInputInvalidData() {
        String one = "0", two = "6", three = "100", four = "abcde";

        assertFalse(pi.validateInput(one));
        assertFalse(pi.validateInput(two));
        assertFalse(pi.validateInput(three));
        assertFalse(pi.validateInput(four));

        int iOne = 0, iTwo = -10, iThree = 100, iFour = 1234;

        assertFalse(pi.validateInput(iOne));
        assertFalse(pi.validateInput(iTwo));
        assertFalse(pi.validateInput(iThree));
        assertFalse(pi.validateInput(iFour));
    }

    @Test
    public void testGetUnitFromConsole() throws IOException {
        when(scanner.readLine()).thenThrow(Exception.class).thenReturn("Blää").thenReturn("2");

        Assert.assertEquals(Regiment.UnitType.Cannons, pi.getUnitFromConsole(new ArrayList<Regiment>()).getUnitType());
    }

    @Test
    public void testExceptionHandling() throws IOException, InterruptedException {
        pi = spy(pi);
        when(scanner.readLine()).thenThrow(IOException.class).thenThrow(IOException.class).thenReturn("n");
        doThrow(InterruptedException.class).when(pi).sleep();
        pi.printNewGameScreen();
        pi.printGameOver(GameState.GameDraw);
        pi.fightFeedback(new AI(10), new AI(10));
    }

    @Test
    public void testEnemyUnitViewGenerator() {
        pi = spy(pi);

        AI ai = new AI(10);
        ai.generateRegiments();

        doReturn(3).doReturn(6).doReturn(9).when(pi).randomAiRegiment(anyList());

        Regiment[] r = ai.getRegiments();

        pi.unitViewPrinter(ai);

        verify(console).println(String.format("Enemy Army: [?][?][?][%s][?][?][%s][?][?][%s]",
                r[3].getUnitType(), r[6].getUnitType(), r[9].getUnitType()));
        verify(pi, times(3)).randomAiRegiment(anyList());
    }

    @Test
    public void testPlayerUnitViewGenerator() {
        pi = spy(pi);

        Player player = new Player(10);
        AI ai = new AI(10);
        ai.generateRegiments();

        player.setRegiments(new ArrayList<>(Arrays.asList(ai.getRegiments())));
        Regiment[] r = player.getRegiments();

        pi.unitViewPrinter(player);
        verify(console).println(String.format("Your Army: [%s][%s][%s][%s][%s][%s][%s][%s][%s][%s]",
                r[0].getUnitType(), r[1].getUnitType(), r[2].getUnitType(), r[3].getUnitType(), r[4].getUnitType(),
                r[5].getUnitType(), r[6].getUnitType(), r[7].getUnitType(), r[8].getUnitType(), r[9].getUnitType()));

        player = new Player(5);
        ai = new AI(5);
        ai.generateRegiments();

        player.setRegiments(new ArrayList<>(Arrays.asList(ai.getRegiments())));
        r = player.getRegiments();

        pi.unitViewPrinter(player);
        verify(console).println(String.format("Your Army: [%s][%s][%s][%s][%s]",
                r[0].getUnitType(), r[1].getUnitType(), r[2].getUnitType(), r[3].getUnitType(), r[4].getUnitType()));
    }

    @Test
    public void testRandomAiRegiment() {
        pi.random = spy(new Random());
        Random rand = new Random();
        int[] iList = {rand.nextInt(10), rand.nextInt(10), rand.nextInt(10)};
        while (iList[0] == iList[1])
            iList[1] = rand.nextInt(10);
        while (iList[0] == iList[2] || iList[1] == iList[2])
            iList[2] = rand.nextInt(10);

        doReturn(iList[0]).doReturn(iList[0]).doReturn(iList[1]).doReturn(iList[1]).doReturn(iList[2]).when(pi.random).nextInt(10);

        AI ai = new AI(10);
        ai.generateRegiments();

        List<Integer> i = new ArrayList<>();
        i.add(pi.randomAiRegiment(i));
        i.add(pi.randomAiRegiment(i));
        i.add(pi.randomAiRegiment(i));

        assertTrue(new ArrayList<>(Arrays.asList(ai.getRegiments())).contains(ai.getRegiments()[i.get(0)]));
        assertTrue(new ArrayList<>(Arrays.asList(ai.getRegiments())).contains(ai.getRegiments()[i.get(1)]));
        assertTrue(new ArrayList<>(Arrays.asList(ai.getRegiments())).contains(ai.getRegiments()[i.get(2)]));

        assertNotEquals(i.get(0).intValue(), i.get(1).intValue());
        assertNotEquals(i, i.get(2));
        assertNotEquals(i.get(1), i.get(2));

        assertEquals(i.get(0).intValue(), iList[0]);
        assertEquals(i.get(1).intValue(), iList[1]);
        assertEquals(i.get(2).intValue(), iList[2]);

        verify(pi.random, times(5)).nextInt(10);
    }

    @Test(expected = IndexOutOfBoundsException.class, timeout = 2000)
    public void testRandomAiRegimentTooManyRegiments() {
        ArrayList<Integer> usedRegiments = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            usedRegiments.add(i);
        }
        pi.randomAiRegiment(usedRegiments);
    }

    @Test
    public void testFightFeedback() throws InterruptedException {
        pi = spy(pi);
        pi.fightFeedback(new AI(10), new AI(10));

        verify(console).println(anyString());
        verify(pi).sleep();
    }

    private ArrayList<Regiment> assertRegimentFromInput(Regiment.UnitType ut, ArrayList<Regiment> regimentList) {
        Regiment r;
        regimentList.add(r = pi.getUnitFromConsole(regimentList));
        assertEquals(ut, r.getUnitType());
        return regimentList;
    }
}
